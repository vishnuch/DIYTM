#!/bin/bash

interface="wlp2s0"

laptopIPv4=$(/sbin/ip -o -4 addr list $interface | awk 'NR==1{print $4}' |cut -d/ -f1)
numbits=$(/sbin/ip -o -4 addr list wlp2s0 | awk 'NR==1{print $4}' | cut -d/ -f2)
laptopUsername="vishnu"

piHostname="vishnuRPi"
piIP="192.168.43.99"
piUsername="pi"

backupDirectories=('/opt' '/home' '/usr' '/etc' '/var');

ssh $piUsername@$piHostname "backup $laptopUsername $laptopIPv4 ${backupDirectories[@]}";	

