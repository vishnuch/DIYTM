#!/bin/bash

read -p "Would you like to start the backup?(Y/n)" -t 30 reply;
echo ;
case $reply in 
	[Yy]* )	echo "Starting backup in 5 seconds!";
		sleep 5;
		initiateBackup;;
	[Nn]* ) echo "Okay, will try again later!";
		sleep 2;;
	* ) 	echo "Okay, will try again later, busy boy! ;)";
		sleep 2;;
esac

