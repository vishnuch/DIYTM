#!/bin/bash

interface="wlp2s0"
laptopIPv4=$(/sbin/ip -o -4 addr list $interface | awk 'NR==1{print $4}' |cut -d/ -f1)
numbits=$(/sbin/ip -o -4 addr list wlp2s0 | awk 'NR==1{print $4}' | cut -d/ -f2)
laptopUsername="vishnu"
piHostname="vishnuRPi"
piIP="192.168.43.99"
piUsername="pi"

isOnline=`nmap -sn $laptopIPv4/$numbits | grep -ie pi -e $piIP`
if [[ -n isOnline ]]; then
		export DISPLAY=:0; gnome-terminal -- /bin/bash -c 'promptBackup'
fi

